import React, { Component } from "react";
import './App.css';
import { Link } from 'react-router-dom';


class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			apiResponse: "",
			isSportsClicked: false,
			isMusicClicked: false,
			isMoviesClicked: false,
			isScienceClicked: false,
		};
	}

	handleSportsClick = () => {
		this.setState(prevState => ({
			isSportsClicked: !prevState.isSportsClicked
		}));
	}

	handleMusicClick = () => {
		this.setState(prevState => ({
			isMusicClicked: !prevState.isMusicClicked
		}));
	}

	handleMoviesClick = () => {
		this.setState(prevState => ({
			isMoviesClicked: !prevState.isMoviesClicked
		}));
	}

	handleScienceClick = () => {
		this.setState(prevState => ({
			isScienceClicked: !prevState.isScienceClicked
		}));
	}

	render() {
		return (
			<div className="App">

				<header className="App-header">
					<h1 className="App-title">Categories</h1>

					<div className="categories">
					
						<div className="category" onClick={this.handleSportsClick}>Sports</div>

						{this.state.isSportsClicked && (
							<div className="subcategories-container">
								<div className="subcategories-links">
									<Link to="/sports/football" className="subcategory-link">Football</Link>
									<Link to="/sports/formula1" className="subcategory-link">Formula 1</Link>
									<Link to="/sports/basketball" className="subcategory-link">Basketball</Link>
									<Link to="/sports/tennis" className="subcategory-link">Tennis</Link>
									<Link to="/sports/volleyball" className="subcategory-link">Volleyball</Link>
									<Link to="/sports/handball" className="subcategory-link">Handball</Link>
								</div>
							</div>
						)}

						<br />

						<div className="category" onClick={this.handleMusicClick}>Music</div>

						{this.state.isMusicClicked && (
							<div className="subcategories-container">
								<div className="subcategories-links">
									<Link to="/music/pop" className="subcategory-link">Pop</Link>
									<Link to="/music/rock" className="subcategory-link">Rock</Link>
									<Link to="/music/edm" className="subcategory-link">EDM</Link>
								</div>
							</div>
						)}

						<br />

						<div className="category" onClick={this.handleMoviesClick}>Movies</div>

						{this.state.isMoviesClicked && (
							<div className="subcategories-container">
								<div className="subcategories-links">
									<Link to="/movies/drama" className="subcategory-link">Drama</Link>
									<Link to="/movies/comedy" className="subcategory-link">Comedy</Link>
								</div>
							</div>
						)}

						<br />

						<div className="category" onClick={this.handleScienceClick}>Science</div>

						{this.state.isScienceClicked && (
							<div className="subcategories-container">
								<div className="subcategories-links">
									<Link to="/science/geography" className="subcategory-link">Geography</Link>
									<Link to="/science/history" className="subcategory-link">History</Link>
									<Link to="/science/physics" className="subcategory-link">Physics</Link>
									<Link to="/science/chemistry" className="subcategory-link">Chemistry</Link>
									<Link to="/science/astronomy" className="subcategory-link">Astronomy</Link>
								</div>
							</div>
						)}
				
						<p>Art</p>

						<br />
					</div>
				</header>

			</div>
		);
	}
}



export default App;
