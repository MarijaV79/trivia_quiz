import React, { Component } from 'react';
import './Signup.css';
import { Link, Route } from 'react-router-dom';


class Signup extends Component {

	constructor(props) {
		super(props);
		this.state = { 
			username: null,
			email: null,
			password: null,
			repeatPassword: null,
		};
	}

	handleUsernameChange = (e) => {
		this.setState({username: e.target.value});
	}

	handleEmailChange = (e) => {
		this.setState({email: e.target.value});
	}

	handlePasswordChange = (e) => {
		this.setState({password: e.target.value});
	}

	handleRepeatPasswordChange = (e) => {
		this.setState({repeatPassword: e.target.value});
	}

	handleClick = (event) => {
		event.preventDefault();

		if (this.state.username !== null && this.state.username !== "" && this.state.email !== null && this.state.email !== "" &&
			this.state.password !== null && this.state.password !== "" && this.state.repeatPassword !== null && this.state.repeatPassword !== "") {

			let currentTime = new Date();

			if(this.state.password === this.state.repeatPassword){
				const requestOptions = {
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify({ username: this.state.username, email: this.state.email, password: this.state.password, 
						repeatPassword: this.state.repeatPassword, timeCreated: currentTime})
				};

				fetch('http://localhost:9000/signup', requestOptions)
					.then(response => response.json())
					.then(data => {
						if (data.success) {
							// Signup successful
							console.log("Registration was successful.");

							alert("Registration was successful.");
							this.props.history.push('/');
		
						} else {
							// Signup failed, show error message
							console.error("Signup failed:", data.message);
							alert("Signup failed: " + data.message);
						}
					}
				);

			} else {
				alert("Password and repeated password have to be the same.");
			}

		} else {
			alert("There are empty fields.");
		}
	}

	render() {
		return (
			<div className="App">
	
				<header className="App-header">
					<h1 className="signup">Sign up</h1>
		
					<form>
						<p>Username:</p>
						<input type="username" placeholder="Enter Username" onChange={this.handleUsernameChange} maxLength={200} />

						<p>E-mail:</p>
						<input type="email" placeholder="Enter E-mail" onChange={this.handleEmailChange} maxLength={200} />

						<p>Password:</p>
						<input type="password" placeholder="Enter Password" onChange={this.handlePasswordChange} maxLength={200} />
						<input type="password" placeholder="Repeat Password" onChange={this.handleRepeatPasswordChange} maxLength={200} />


						<br></br>
						<button className="signup_button" type="submit" onClick={this.handleClick}>Sign up</button>
					</form>

					<br/>
				</header>

			</div>
		);
    }
}


export default Signup;
