import React, { Component } from 'react';
import './Login.css';
import { withRouter, useHistory, Link, Route } from 'react-router-dom';


class Login extends Component {

	constructor(props) {
		super(props);
		this.state = { 
			username: null,
			password: null,
		};
	}

	handleUsernameChange = (e) => {
		this.setState({username: e.target.value});
	}

	handlePasswordChange = (e) => {
		this.setState({password: e.target.value});
	}

	handleClick = (event) => {
		event.preventDefault();

		if (this.state.username !== null && this.state.username !== "" && this.state.password !== null && this.state.password !== "") {
			const requestOptions = {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({ username: this.state.username, password: this.state.password}),
				credentials: 'include',
			};

			fetch('http://localhost:9000/login', requestOptions)
			.then(response => response.json())
			.then(data => {
				if (data.success) {
					// Login successful, perform actions here
					console.log("Login successful");

					// Redirect and update state
					this.props.updateUserIsLoggedIn();
					this.props.history.push('/');

				} else {
					// Login failed, show error message
					console.error("Login failed:", data.message);
					alert("Login failed: " + data.message);
				}
			})
			.catch(error => {
				// Handle fetch error
				console.error('Error during login:', error);
				alert("Error during login: " + error.message);
			});

		} else {
			alert("Empty username or password.");
		}
	}

	render() {
		return (
			<div className="App">
	
				<header className="App-header">
					<h1 className="login">Login</h1>
		
					<form>
						<p>Username:</p>
						<input type="username" placeholder="Enter Username" onChange={this.handleUsernameChange} />

						<p>Password:</p>
						<input type="password" placeholder="Enter Password" onChange={this.handlePasswordChange} />


						<br></br>
						<button className="login_button" type="submit" onClick={this.handleClick}>Login</button>
					</form>

					<br/>

				</header>

			</div>
		);
	}
}


export default withRouter(Login);
