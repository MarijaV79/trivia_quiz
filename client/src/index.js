import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Question from './Question';
import Submit from './Submit';
import Login from './Login';
import Signup from './Signup';
import Profile from './Profile';
import { BrowserRouter as Router, Switch, Route, Redirect, Link } from 'react-router-dom';

class MainApp extends React.Component {

	constructor(props) {
		super(props);
		this.state = { 
			userIsLoggedIn: false,
		};
	}

	updateUserIsLoggedIn = () => {
		this.setState({ userIsLoggedIn: true });
	};

	handleLogout = (event) => {
		event.preventDefault();

		// Make a request to the server to handle the logout
		fetch('http://localhost:9000/logout', {
			method: 'GET',
			credentials: 'include', // Include cookies in the request
		})
		.then(response => {
			if (!response.ok) {
				throw new Error('Logout request failed');
			}
			return response.json();
		})
		.then(data => {
			console.log('Logout successful:', data);
			this.setState({ userIsLoggedIn: false });
		})
		.catch(error => {
			console.error('Logout error:', error);
			// Handle the error
		});
	};

	render() {
		return (
		<div>
			<Router>
				<div className="topnav">
					{this.state.userIsLoggedIn && <Link to="/" className="active">Home</Link>}
					{!this.state.userIsLoggedIn && <Link to="/login" className="active">Login</Link>}
					{!this.state.userIsLoggedIn && <Link to="/signup" className="active">Sign up</Link>}
					{this.state.userIsLoggedIn && <Link to="/submit" className="active">Submit questions</Link>}
					{this.state.userIsLoggedIn && <Link to="/profile" className="active">Profile</Link>}
					{this.state.userIsLoggedIn && <a href="#" className="active logout" onClick={this.handleLogout}>Log out</a>}
				</div>

				<Switch>
					<Route
						exact
						path="/"
						component={() => this.state.userIsLoggedIn ? <App /> : <Redirect to="/login" />}
					/>

					<Route
						exact
						path="/submit"
						component={() => this.state.userIsLoggedIn ? <Submit /> : <Redirect to="/login" />}
					/>

					<Route
						exact
						path="/profile"
						component={() => this.state.userIsLoggedIn ? <Profile /> : <Redirect to="/login" />}
					/>

					<Route
						exact
						path="/login"
						component={() => <Login updateUserIsLoggedIn={this.updateUserIsLoggedIn} />}
					/>
					<Route exact path="/signup" component={Signup}/>

					<Route exact path="/sports/football" component={Question }/>
					<Route exact path="/sports/basketball" component={Question }/>
					<Route exact path="/sports/handball" component={Question }/>
					<Route exact path="/sports/tennis" component={Question }/>
					<Route exact path="/sports/formula1" component={Question }/>
					<Route exact path="/sports/volleyball" component={Question }/>

					<Route exact path="/music/pop" component={Question }/>
					<Route exact path="/music/rock" component={Question }/>
					<Route exact path="/music/edm" component={Question }/>

					<Route exact path="/movies/drama" component={Question }/>
					<Route exact path="/movies/comedy" component={Question }/>

					<Route exact path="/science/geography" component={Question }/>
					<Route exact path="/science/history" component={Question }/>
					<Route exact path="/science/physics" component={Question }/>
					<Route exact path="/science/chemistry" component={Question }/>
					<Route exact path="/science/astronomy" component={Question }/>
				</Switch>
			</Router>

			<div className="bottomnav">
			{/* Content */}
			</div>
		</div>
		);
	}
}

ReactDOM.render(<MainApp />, document.getElementById('root'));
reportWebVitals();
