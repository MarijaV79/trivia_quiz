import React, { Component } from "react";
import './App.css';


class Submit extends Component {
	constructor(props) {
		super(props);
		this.state = {
			question: "",
			answer1: "",
			answer2: "",
			answer3: "",
			answer4: "",
			correct: "",
			category: 'empty',
			subcategory: 'empty',
			subcategories: {
				empty: ['...'],
				sports: ['...', 'Football', 'Formula1', 'Basketball', 'Tennis'],
				music: ['...', 'Pop', 'Rock', 'EDM']
			},
			emptyValues: ['', '...', 'empty', null, undefined]
		};
	}

	handleClick = (event) => {
		event.preventDefault();

		if (!this.state.emptyValues.includes(this.state.question) && !this.state.emptyValues.includes(this.state.answer1) &&
			!this.state.emptyValues.includes(this.state.answer2) && !this.state.emptyValues.includes(this.state.answer3) &&
			!this.state.emptyValues.includes(this.state.answer4) && !this.state.emptyValues.includes(this.state.correct) &&
			!this.state.emptyValues.includes(this.state.category) && !this.state.emptyValues.includes(this.state.subcategory)) {

			if (this.state.correct.trim() !== this.state.answer1.trim() && this.state.correct.trim() !== this.state.answer2.trim() &&
				this.state.correct.trim() !== this.state.answer3.trim() && this.state.correct.trim() !== this.state.answer4.trim()) {
				alert("The correct question should be among one of the 4 answers.");
				return;
			}

			const requestOptions = {
				method: 'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify({ question: this.state.question.trim(), answer1: this.state.answer1.trim(),
								answer2: this.state.answer2.trim(), answer3: this.state.answer3.trim(), answer4: this.state.answer4.trim(), 
								correct: this.state.correct.trim(), category: this.state.category, subcategory: this.state.subcategory })
			};

			fetch('http://localhost:9000/fetch', requestOptions)
				.then(response => response.json())
				.then(data => {
					if (data.success === true) {
						//window.location.reload();

						this.setState({
							question: "",
							answer1: "",
							answer2: "",
							answer3: "",
							answer4: "",
							correct: "",
							category: "empty",
							subcategory: "empty",
						});

						alert("The question was successfully submitted.");

					} else {
						alert("Submit error.");
					}
				}
			);

		} else {
			alert("There are empty fields.");
		}
	}

	handleQuestionChange = (e) => {
		this.setState({question: e.target.value});
	}

	handleAnswer1Change = (e) => {
		this.setState({answer1: e.target.value});
	}

	handleAnswer2Change = (e) => {
		this.setState({answer2: e.target.value});
	}

	handleAnswer3Change = (e) => {
		this.setState({answer3: e.target.value});
	}

	handleAnswer4Change = (e) => {
		this.setState({answer4: e.target.value});
	}

	handleCorrectChange = (e) => {
		this.setState({correct: e.target.value});
	}

	handleCategoryChange = (e) => {
		this.setState({category: e.target.value});  
	}

	handleSubcategoryChange = (e) => {
		this.setState({subcategory: e.target.value});  
	}


	render() {
		return (
			<div className="App">

				<header className="submit_question">

					<h1>Submit a question</h1>
				
					<form>
						<p>Enter a question:</p>
						<input type="text" placeholder="Enter a question" value={this.state.question} onChange={this.handleQuestionChange} />

						<p>Enter answer 1:</p>
						<input type="text" placeholder="Enter answer 1" value={this.state.answer1} onChange={this.handleAnswer1Change} />

						<p>Enter answer 2:</p>
						<input type="text" placeholder="Enter answer 2" value={this.state.answer2} onChange={this.handleAnswer2Change} />

						<p>Enter answer 3:</p>
						<input type="text" placeholder="Enter answer 3" value={this.state.answer3} onChange={this.handleAnswer3Change} />

						<p>Enter answer 4:</p>
						<input type="text" placeholder="Enter answer 4" value={this.state.answer4} onChange={this.handleAnswer4Change} />

						<p>Enter correct answer:</p>
						<input type="text" placeholder="Enter correct answer" value={this.state.correct} onChange={this.handleCorrectChange} />

						<br />
						<br />

						<div className="container">
							<label className="select-label">
								Choose a category:
								<select value={this.state.category} onChange={this.handleCategoryChange} className="select-box">
									<option value="empty">...</option>
									<option value="sports">Sports</option>
									<option value="music">Music</option>
								</select>
							</label>

							<br />

							<label className="select-label">
								Choose a subcategory:
								<select value={this.state.subcategory} onChange={this.handleSubcategoryChange} className="select-box">
									{this.state.subcategories[this.state.category].map((option, index) => (
										<option key={index} value={option}>{option}</option>
									))}
								</select>
							</label>
						</div>


						<input type="submit" value="Submit" onClick={this.handleClick} />
					</form>

				</header>

			</div>
		);
	}
}


export default Submit;
