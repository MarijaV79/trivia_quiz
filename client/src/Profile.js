import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './Profile.css';


class Profile extends Component {

	constructor(props) {
		super(props);
		this.state = { 
			userData: {
				username: null,
				email: null,
				timeCreated: null,
			},
			userStatistics: {
				quizzesTaken: null,
				questionsAnswered: null,
				correctAnswers: null,
				incorrectAnswers: null,
			},
		};
	}

	componentDidMount() {
		this.getUserData();
	}

	getUserData(){
		fetch('http://localhost:9000/userData', {
			method: 'GET',
			credentials: 'include', // Include cookies in the request
		})
		.then(res => {
			if (!res.ok) {
				throw new Error("Network response was not ok");
			}
			return res.text();
		})
		.then(res => {
			let userDataJSON = JSON.parse(res);
			console.log("Received data:", userDataJSON);

			let userData = userDataJSON.userData;
			let userStatistics = userDataJSON.userStatistics;

			this.setState({ 
				userData: userData,
				userStatistics: userStatistics,
			});
		})
		.catch(error => {
			console.error("Error fetching data:", error);
			
			// Handle the error here, e.g., display a message to the user
			alert("There was a problem fetching user data from the database.");
		});
	}

	render() {
		return (
			<div className="user-statistics">
				<header className="App-header">

					<div className="statistics-container">
						<h2>User Data</h2>
						<table className="statistics-table">
							<tbody>
								<tr>
									<th>Username</th>
									<td>{this.state.userData.username}</td>
								</tr>
								<tr>
									<th>Email</th>
									<td>{this.state.userData.email}</td>
								</tr>
								<tr>
									<th>Time Created</th>
									<td>{this.state.userData.timeCreated}</td>
								</tr>
							</tbody>
						</table>
					</div>


					<div className="statistics-container">
						<h2>User Statistics</h2>
						<table className="statistics-table">
							<tbody>
								<tr>
									<th>Quizzes Taken</th>
									<td>{this.state.userStatistics.quizzesTaken}</td>
								</tr>
								<tr>
									<th>Questions Answered</th>
									<td>{this.state.userStatistics.questionsAnswered}</td>
								</tr>
								<tr>
									<th>Correct Answers</th>
									<td>{this.state.userStatistics.correctAnswers}</td>
								</tr>
								<tr>
									<th>Incorrect Answers</th>
									<td>{this.state.userStatistics.incorrectAnswers}</td>
								</tr>
								<tr>
									<th>Accuracy</th>
									<td>
									{this.state.userStatistics.questionsAnswered === 0 || this.state.userStatistics.questionsAnswered === "0" ||
									this.state.userStatistics.questionsAnswered === null || this.state.userStatistics.questionsAnswered === undefined
            							? 'N/A' 
            							: ((this.state.userStatistics.correctAnswers / this.state.userStatistics.questionsAnswered) * 100).toFixed(2) + '%'}
									</td>
								</tr>
							</tbody>
						</table>
					</div>


					<br/>

				</header>

			</div>
		);
	}
}


export default withRouter(Profile);
