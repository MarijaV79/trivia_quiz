import React, { Component } from 'react';
import './index.css';


class Question extends Component {
	constructor (props) {
		super (props);
		this.state = {
			questionNumber: 0,
			numberOfQuestions: 3,
			questionIndex: 0,
			usedQuestionIndexes: [],
			bgColor: ['gray', 'gray', 'gray', 'gray'],
			score: 0,
			isDisabled: false,
			gameIsOver: false,
			answerIsClicked: false,
			listOfQuestions: [{
				subcategory: 'subcategory',
				question: 'question',
				answer1: 'answer1',
				answer2: 'answer2',
				answer3: 'answer3',
				answer4: 'answer4',
				correct: 'correct'
			}],
			quizzesTaken: 1,
		}
	}

	componentDidMount() {
		this.getQuestions();
	}

	getQuestions(){
		fetch("http://localhost:9000/fetch" + this.props.location.pathname)
		.then(res => {
			if (!res.ok) {
				throw new Error("Network response was not ok");
			}
			return res.text();
		})
		.then(res => {
			console.log("Received data text:", res);
			let questionsJSON = JSON.parse(res);
			console.log("Received data:", questionsJSON);

			let max = questionsJSON.length;
			let index = this.getRandomNumber(max);

			this.state.usedQuestionIndexes.push(index);

			this.setState({ 
				listOfQuestions: questionsJSON,
				questionIndex: index,
			});
		})
		.catch(error => {
			console.error("Error fetching data:", error);
			
			// Handle the error here, e.g., display a message to the user
			alert("There was a problem fetching questions from the database.");

			// Redirect to home page
			this.props.history.push('/');
		});
	}

	updateUserStatistics = () => {
		let incorrectAnswers = this.state.numberOfQuestions - this.state.score;

		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			credentials: 'include',
			body: JSON.stringify({ quizzesTaken: this.state.quizzesTaken, questionsAnswered: this.state.numberOfQuestions,
								correctAnswers: this.state.score, incorrectAnswers: incorrectAnswers })
		};

		fetch('http://localhost:9000/userStatistics', requestOptions)
			.then(response => response.json())
			.then(data => {
				console.log("Data:", data);

				if (data.success === true) {
					console.log("Success");

					// Redirect to home page
					this.props.history.push('/');

				} else {
					console.log("Error.");
				}
			}
		);
	}

	getRandomNumber(max) {
		// Generate a random number between 0 (inclusive) and 1 (exclusive)
		const randomNumber = Math.random();
		
		// Scale the random number to the range [0, max)
		const scaledNumber = randomNumber * max;

		// Convert the scaled number to an integer
		const randomNumberInRange = Math.floor(scaledNumber);
		
		return randomNumberInRange;
	}

	handleClickAnswer = (chosenAnswer, num) => {
		let bgColor;
		
		// Check if answer is correct
		if (chosenAnswer === this.state.listOfQuestions[this.state.questionIndex].correct){
			bgColor = this.state.bgColor;
			bgColor[num-1] = "green";

			this.setState({
				bgColor: bgColor,
				score: this.state.score + 1,
				isDisabled: true,
				answerIsClicked: true,
			});

		} else {
			bgColor = this.state.bgColor;
			bgColor[num-1] = "red";

			this.setState({
				bgColor: bgColor,
				isDisabled: true,
				answerIsClicked: true,
			});
		}
	}

	calculateNextQuestionIndex() {
		let max = this.state.listOfQuestions.length;
		let index = this.getRandomNumber(max);

		if (this.state.usedQuestionIndexes.includes(index)) {
			return this.calculateNextQuestionIndex();
		} else {
			return index;
		}
	}

	getNextQuestion = () => {
		let bgColor = ['gray', 'gray', 'gray', 'gray'];

		if (this.state.questionNumber < this.state.numberOfQuestions - 1){
			let index = this.calculateNextQuestionIndex();
			this.state.usedQuestionIndexes.push(index);

			this.setState({
				questionNumber: this.state.questionNumber + 1,
				bgColor: bgColor,
				isDisabled: false,
				questionIndex: index,
				answerIsClicked: false,
			});

		} else {
			this.setState({
				gameIsOver: true
			});
		}
	}

	render() {
		return (
		<div className="App">

			<header className="App-header">
				<h1 className="App-title">Question {this.state.questionNumber + 1}/3</h1>

				<div className="categories">
					<p>{this.state.listOfQuestions[this.state.questionIndex].question}</p>

					<table border="0" id="answers">
						<tbody>
							<tr>
								<td>
									<button 
										className="answer_btn" 
										style={{backgroundColor: this.state.bgColor[0]}} 
										onClick={() => this.handleClickAnswer(this.state.listOfQuestions[this.state.questionIndex].answer1, 1)} 
										disabled={this.state.isDisabled}
									> 
										{this.state.listOfQuestions[this.state.questionIndex].answer1} 
									</button>
								</td>
							</tr>

							<tr>
								<td>
									<button 
										className="answer_btn" 
										style={{backgroundColor: this.state.bgColor[1]}} 
										onClick={() => this.handleClickAnswer(this.state.listOfQuestions[this.state.questionIndex].answer2, 2)} 
										disabled={this.state.isDisabled}
									> 
										{this.state.listOfQuestions[this.state.questionIndex].answer2} 
									</button>
								</td>
							</tr>

							<tr>
								<td>
									<button 
										className="answer_btn" 
										style={{backgroundColor: this.state.bgColor[2]}} 
										onClick={() => this.handleClickAnswer(this.state.listOfQuestions[this.state.questionIndex].answer3, 3)} 
										disabled={this.state.isDisabled}
									> 
										{this.state.listOfQuestions[this.state.questionIndex].answer3} 
									</button>
								</td>
							</tr>

							<tr>  
								<td>
									<button 
										className="answer_btn" 
										style={{backgroundColor: this.state.bgColor[3]}} 
										onClick={() => this.handleClickAnswer(this.state.listOfQuestions[this.state.questionIndex].answer4, 4)} 
										disabled={this.state.isDisabled}
									> 
										{this.state.listOfQuestions[this.state.questionIndex].answer4} 
									</button>
								</td>
							</tr>

						</tbody>
					</table>

					<br></br>
					{!this.state.gameIsOver && 
						<button 
							className="next_question" 
							onClick={this.getNextQuestion} 
							disabled={!this.state.answerIsClicked}
						>
							Next
						</button>
					}

					{this.state.gameIsOver && 
						<button className="next_question" onClick={this.updateUserStatistics}>Exit</button>
					}

					{this.state.gameIsOver && <p>Score: {this.state.score}</p>}

				</div>
			</header>

		</div>
		);
	}
}
  
  

export default Question;
