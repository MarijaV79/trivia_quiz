# TriviaQuiz App

TriviaQuiz is a full-stack web application that allows users to participate in trivia quizzes on various topics. The application is built using Node.js for the backend and React.js for the frontend.

## Features

- User authentication and authorization
- Various trivia categories and questions
- Scoring system to track user performance
- Users can submit new questions

## Technologies Used

- **Frontend:** React.js
- **Backend:** Node.js, Express.js
- **Database:** PostgreSQL
- **Styling:** CSS


## Getting started

To get a local copy of the project up and running, follow these steps.

### Prerequisites

Ensure you have the following installed:

- Node.js
- npm (Node Package Manager)
- PostgreSQL


### Installation

#### Clone the repository
```bash
git clone https://gitlab.com/MarijaV79/trivia_quiz.git
cd trivia_quiz
```

### Running the Application

#### Start the PostgreSQL server
```bash
sudo service postgresql start
```

#### Start the backend server
```bash
cd api
npm start
```

#### Start the frontend development server
```bash
cd client
npm start
```
