var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var crypto = require('crypto-js'); 

require('dotenv').config({path: `/home/marija/project2/api/process.env`});
const pool = require('/home/marija/project2/api/routes/db_connection.js');


const generateAuthToken = () => {
	return crypto.lib.WordArray.random(32);
}

const authTokens = {};


/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', { title: 'Express' });
});


// Login
router.post('/login', function(req, res, next) {
	console.log("POST login");
	console.log(req.body);

	let username = req.body.username;
	let password = req.body.password;
	let user = "";


	pool.query('SELECT * FROM users', (err, res2) => {
		if (err) {
			console.error(err);
			return;
		}

		let users = res2.rows;
		let iterations = users.length;

		for (let row of users) {
			let pwd = row.password;
			console.log("password:", pwd);

			if(username === row.username){
				bcrypt.compare(password, pwd, function (err, result) {
					if (result == true) {
						console.log("Correct password.");
						console.log(username);
						console.log(password);
						
						user = username;
						console.log("User:", user);

						const authToken = generateAuthToken();
						authTokens[authToken] = user;
						res.cookie('AuthToken', authToken, { secure: true, httpOnly: true });

						console.log("authTokens:");
						console.log(authTokens);

						// Send JSON response to frontend
						res.status(200).json({ success: true, user: user, authToken: authToken });
						return;  // Return to avoid sending multiple responses

					} else {
						console.log("Incorrect password.");
			
						if (!--iterations){
							res.status(401).json({ success: false, message: 'Wrong username and/or password.' });
						}
					}
				});
			} else{
				if (!--iterations){
					res.status(401).json({ success: false, message: 'Wrong username and/or password.' });
				}
			}
		}
	});
});


// Logout
router.get('/logout', function(req, res, next) {
	try {
		const authToken = req.cookies['AuthToken'];
		console.log("authToken:", authToken);

		var hex = crypto.enc.Hex.stringify(authToken);
		console.log("hex:", hex);

		delete authTokens[hex];
		console.log("authTokens:", authTokens);

		res.status(200).json({ success: true });

	} catch (error) {
		console.error('Error during logout:', error);
		res.status(500).json({ success: false, error: 'Internal Server Error' });
	}
});


// Sign up
router.post('/signup', function(req, res, next) {
	console.log("POST signup");
	console.log(req.body);
	let password = req.body.password;

	let hashPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
	console.log("hashPassword:", hashPassword);

	pool.query(
		'INSERT INTO "users" (username, email, password, time_created) VALUES ($1, $2, $3, $4)',
		[req.body.username, req.body.email, hashPassword, req.body.timeCreated],
		(err, res2) => {
			console.log(err, res2);

			if (err === undefined){
				console.log("Registration was successful.");
				res.status(200).json({ success: true });
			} else {
				console.log("Database error!");
				res.status(500).json({ success: false, error: 'Internal Server Error' });
			}
		}
	);
});


// Get user data and statistics from the database
router.get('/userData', function(req, res, next) {
	console.log(req.originalUrl);

	const authToken = req.cookies['AuthToken'];
	let hex = crypto.enc.Hex.stringify(authToken);
	let username = authTokens[hex];

	let userData = null;
	let userStatistics = null;

	const query = `
		SELECT u.username, u.email, u.time_created, us.quizzes_taken, us.questions_answered, us.correct_answers, us.incorrect_answers
		FROM users u
		INNER JOIN user_statistics us ON u.id = us.user_id
		WHERE username = $1;
	`;

	pool.query(query, [username], (err, result) => {
		if (err) {
			console.error("Error executing query:", err);
			return res.status(500).send("Internal Server Error");
		}

		if (result.rows.length === 0) {
			console.log("No data found for username:", username);
			return res.status(404).send("No data found");
		}

		console.log("Data retrieved successfully:", result.rows);
		let data = result.rows[0];
		userData =  {username: data.username, email: data.email, timeCreated: new Date(data.time_created).toLocaleString()};
		userStatistics = {quizzesTaken: data.quizzes_taken, questionsAnswered: data.questions_answered, correctAnswers: data.correct_answers, 
						incorrectAnswers: data.incorrect_answers};
		console.log("userData:", userData);

		res.status(200).send({userData: userData, userStatistics: userStatistics});
	});
});


// Update user statistics
router.post('/userStatistics', async function(req, res, next) {
	console.log("userStatistics");
	console.log(req.body);

	const authToken = req.cookies['AuthToken'];
	let hex = crypto.enc.Hex.stringify(authToken);
	let username = authTokens[hex];

	try {
		// Query to get user_id based on username
		const userIdQuery = 'SELECT id FROM users WHERE username = $1';
		const userIdResult = await pool.query(userIdQuery, [username]);

		if (userIdResult.rows.length === 0) {
			res.status(404).json({ success: false, message: 'User not found' });
			return;
		}

		let user_id = userIdResult.rows[0].id;
		let quizzes_taken = req.body.quizzesTaken;
		let questions_answered = req.body.questionsAnswered;
		let correct_answers = req.body.correctAnswers;
		let incorrect_answers = req.body.incorrectAnswers;

		const query = `
			INSERT INTO user_statistics (user_id, quizzes_taken, questions_answered, correct_answers, incorrect_answers)
			VALUES ($1, $2, $3, $4, $5)
			ON CONFLICT (user_id)
			DO UPDATE SET
				quizzes_taken = user_statistics.quizzes_taken + EXCLUDED.quizzes_taken,
				questions_answered = user_statistics.questions_answered + EXCLUDED.questions_answered,
				correct_answers = user_statistics.correct_answers + EXCLUDED.correct_answers,
				incorrect_answers = user_statistics.incorrect_answers + EXCLUDED.incorrect_answers;
		`;

		await pool.query(query, [user_id, quizzes_taken, questions_answered, correct_answers, incorrect_answers]);

		console.log("User statistics have been successfully updated.");
    	res.status(200).json({ success: true });

	} catch (err) {
		console.error('Error executing query', err.stack);
		res.status(500).json({ success: false, error: err.message });
	}
});


module.exports = router;
