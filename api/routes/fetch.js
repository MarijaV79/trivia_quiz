var express = require('express');
var router = express.Router();
require('dotenv').config({path: `/home/marija/project2/api/process.env`});

const pool = require('/home/marija/project2/api/routes/db_connection.js');


// Get questions from the database
router.get('/', function(req, res, next) {
	console.log(req.originalUrl);

	let category = req.originalUrl.split("/")[2];
	let subcategory = req.originalUrl.split("/")[3];

	if (!category || !subcategory) {
		return res.status(400).send("Invalid category or subcategory");
	}

	pool.query(`SELECT * FROM ${category} WHERE subcategory = $1`, [subcategory], (err, result) => {
		if (err) {
			console.error("Error executing query:", err);
			return res.status(500).send("Internal Server Error");
		}

		if (result.rows.length === 0) {
			console.log("No data found for category:", category, "and subcategory:", subcategory);
			return res.status(404).send("No data found");
		}

		if (result.rows.length < 3) {
			console.log("Not enough questions for subcategory:", subcategory);
			return res.status(404).send("Not enough questions for subcategory");
		}

		console.log("Data retrieved successfully:", result.rows);
		res.status(200).send(result.rows);
	});
});


// Submit a question
router.post('/', function(req, res, next) {
	console.log("POST");
	console.log(req.body);
	let question = req.body.question;
	let answer1 = req.body.answer1;
	let answer2 = req.body.answer2;
	let answer3 = req.body.answer3;
	let answer4 = req.body.answer4;
	let correct = req.body.correct;
	let category = req.body.category.toLowerCase();
	let subcategory = req.body.subcategory.toLowerCase();
	console.log(category);


	pool.query(
		`INSERT INTO ${category} (subcategory, question, answer1, answer2, answer3, answer4, correct) VALUES ($1, $2, $3, $4, $5, $6, $7)`, 
		[subcategory, question, answer1, answer2, answer3, answer4, correct],
		(err, res2) => {
			console.log(err, res2);

			if (err === undefined){
				console.log("Success.");
				res.status(200).json({ success: true });
			} else {
				console.log("ERROR!");
				res.status(500).json({ success: false });
			}
		}
	);
});


module.exports = router;